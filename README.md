## NAME
OpenCV Image Processing

## DESCRIPTION
Assignment 5: Iot Project 2: Processing Images

## INSTRUCTIONS
How to run this on your own pc?

Make sure you have installed following packages/tools:
1. g++ (version 11)
2. cmake
3. vcpkg
4. restclient-cpp
5. nlohmann-json
6. OpenCV

```sh
$ git init
```
```sh
$ git clone https://gitlab.com/tinonum/opencv-demo
```
```sh
$ mkdir build && cd build
```
```sh
$ cmake../
```
```sh
$ make
```
```sh
$ ./project-5
```

## PROGRAM FAQ
Program gets random image of a cat from aws.random.cat api and processes it with few different techniques.
1. resize
2. turns picture black and white
3. putText
4. Gaussian blur

Program saves original images in original folder and modified images in output folder.

This repository comes with 10 examples. Check original and output folders.

## MAINTEINERS
Tino Nummela | [@tinonum](https://gitlab.com/tinonum)

Juho Kangas | [@jhkangas3](https://gitlab.com/jhkangas3)

**************************