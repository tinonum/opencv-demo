// AUTHORS: Tino Nummela, Juho Kangas

#include <iostream>
#include <string>
#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <restclient-cpp/restclient.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

int main(int, char **)
{

    // GET image url from api
    RestClient::Response r = RestClient::get("https://aws.random.cat/meow");
    auto json_body = json::parse(r.body);
    std::string url = json_body["file"];

    // Determine filename string for downloaded image
    std::size_t pos = url.rfind("/");
    std::string filename = url.substr(pos + 1);

    // Checking if downloaded image is type .gif and discarding it incase it is.
    pos = filename.rfind(".");
    std::string filetype = filename.substr(pos);
    if (filetype == ".gif")
    {
        std::cout << "File type is gif. Cannot handle that. Exiting program" << std::endl;
        return 0;
    }

    // Determine filebody and rename downloaded image
    std::string filebody = filename.substr(0, pos);
    std::string name_orig = filebody + "_orig" + filetype;

    // Name for new modified image
    std::string name_new = filebody + "_new" + filetype;

    // Download image
    std::string folder_path = "../original/";
    std::string str = "wget "+url+" -P "+folder_path;
    int n = str.length();
    char wgetChar[n + 1];
    strcpy(wgetChar, str.c_str());
    system(strcpy(wgetChar, str.c_str()));

    // Change filename (add _orig)
    str = "mv " + folder_path + filename + " "+ folder_path + name_orig;
    n = str.length();
    char mvChar[n + 1];
    system(strcpy(mvChar, str.c_str()));

    // Read from freshly dowloaded file
    cv::Mat frame = cv::imread("../original/" + name_orig);
    // Checking if image is downloaded correctly
    if (frame.empty())
    {
        std::cout << "Could not read the image: " << std::endl;
        return 0;
    }

    // Determine max size for new modified image
    cv::Mat resized_frame, gray_frame, text_frame, output_frame;
        double imageX = frame.cols;
        double imageY = frame.rows;
        double factor;

        if (imageX >= imageY)
        {
            factor = 300 / imageX;
        }
        else
        {
            factor = 300 / imageY;
        }

    // Resizing new modiefied image, that it stays in same aspect ratio but max side size is 300px
    cv::resize(frame, resized_frame, cv::Size(), factor, factor, cv::INTER_LINEAR);
    // Print new size
    std::cout << "Width: " << resized_frame.cols << std::endl;
    std::cout << "Height: " << resized_frame.rows << std::endl;

    // Make image black and white
    cv::cvtColor(resized_frame, gray_frame, cv::COLOR_BGR2GRAY);
    cv::cvtColor(gray_frame, text_frame, cv::COLOR_GRAY2BGR);

    // Print "Tino Juho" on new modified image
    cv::putText(text_frame,
                "Tino Juho",
                cv::Point(10, 100),
                cv::FONT_HERSHEY_COMPLEX_SMALL,
                1.0,
                cv::Scalar(255, 200, 255),
                1,
                cv::LINE_AA);

    // Gaussian blur to new modiefied image
    cv::Size ksize;
    ksize.width = 5;
    ksize.height = 5;
    cv::GaussianBlur(text_frame, output_frame, ksize, 0, 0, cv::BORDER_DEFAULT);

    // Write new modified image to correct location
    cv::imwrite("../output/" + name_new, output_frame);

    return 0;
}