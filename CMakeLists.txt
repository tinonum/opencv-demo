cmake_minimum_required(VERSION 3.0.0)
project(project-5 VERSION 0.1.0)

find_package(OpenCV REQUIRED)
find_package(restclient-cpp REQUIRED)
find_package(nlohmann_json CONFIG REQUIRED)

LINK_DIRECTORIES(
    /opt/vcpkg/installed/arm64-linux/lib
)

add_executable(project-5 main.cpp)

INCLUDE_DIRECTORIES(
    /opt/vcpkg/installed/arm64-linux/include
    ${OpenCV_INCLUDE_DIRS}
)

TARGET_LINK_LIBRARIES(project-5
    ${OpenCV_LIBS}
    restclient-cpp
    nlohmann_json 
    nlohmann_json::nlohmann_json
)